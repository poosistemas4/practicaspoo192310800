﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog02_P1_U2
{
    class Operacion
    {
        public int m;
        public int n;


        public void Imprime_Matriz(int[,] datos, int n, int m)
        {

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write("  " + datos[i, j]);
                }
                Console.WriteLine();
            }
        }


        public void Leer_Matriz()
        {
            Console.WriteLine("Ingresa Valor de n");
            n = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingresa Valor de m");
            m = int.Parse(Console.ReadLine());
            int[,] datos = new int[n, m];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.WriteLine("Ingresa valor datos [" + i + ", " + j + "]");
                    datos[i, j] = int.Parse(Console.ReadLine());

                }

            }
            Operacion ObjM = new Operacion();
            ObjM.Imprime_Matriz(datos, n, m);
        }
    }
}