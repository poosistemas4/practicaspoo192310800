﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaClase1
{
    class Persona
    {
        private string Nombre;
        private int Edad;
        private string Linea;

        public void Inicializar()
        {
            Console.Write("Ingrese el nombre:");
            Nombre = Console.ReadLine();
            Console.Write("Ingrese la edad:");
            Linea = Console.ReadLine();
            Edad = int.Parse(Linea);
        }

        public void Imprimir()
        {
            Console.Write("Nombre:");
            Console.WriteLine(Nombre);
            Console.Write("Edad:");
            Console.WriteLine(Edad);
        }

        public void EsMayorEdad()
        {
            if (Edad >= 18)
            {
                Console.Write(Nombre+" Es mayor de edad");
            }
            else
            {
                Console.Write(Nombre+" No es mayor de edad");
            }
        }
    }
}
