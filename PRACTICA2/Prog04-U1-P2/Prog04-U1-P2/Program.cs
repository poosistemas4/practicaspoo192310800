﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog04_U1_P2
{
    class Program
    {
        static void Main(string[] args)
        {
            Cliente ObjM = new Cliente();
            ObjM.InsertarCliente();
            ObjM.MostrarCliente();
            ObjM.EliminarCliente();
            Console.ReadKey();
        }
    }
}
