﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog03_U1_P2
{
    class Operaciones
    {
        double PerMon, areaMon, radioMon;
        public void Moneda()
        {
            Console.WriteLine("MONEDA:");
            radioMon = 1;
            PerMon = (3.1416*2)*radioMon;
            Console.WriteLine("PERIMETRO: "+PerMon);
            areaMon = 3.1416 * radioMon * radioMon;
            Console.WriteLine("AREA: "  + areaMon);
        }
        double PerRueda, AreaRueda, RadioRueda;
        public void Rueda()
        {
            Console.WriteLine("RUEDA: ");
            RadioRueda = 10;
            PerRueda = (3.1416*2) *RadioRueda;
            Console.WriteLine("PERIMETRO: " + PerRueda);
            AreaRueda = 3.1416 * RadioRueda * RadioRueda;
            Console.WriteLine("AREA: " + AreaRueda);
        }
    }
}
