﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog02_U1_P2
{
    class Program
    {
        static void Main(string[] args)
        {
            Circunferencia obj = new Circunferencia();
            obj.Calcular_area();
            obj.Calcular_perimetro();
            Console.ReadKey();
        }
    }
}
